import React, { Component } from 'react';

import { 
	AppRegistry, 
	View, 
	Text,
	StatusBar,

	StyleSheet,

	ImageBackground,
	Dimensions
} from 'react-native';

import picYoda from './assets/yoda.jpg';
import picPika from './assets/pika.jpg';
import picKhajit from './assets/khajit.jpeg';

class App extends Component {
	render() {
		return (
			<View style={styles.container}>
				<ImageBackground style={styles.pic} source={picYoda}>
					<Text style={styles.userName}>
						Baby Yoda
					</Text>
				</ImageBackground>
				<ImageBackground style={styles.pic} source={picPika}>
					<Text style={styles.userName}>
						Pika Pika!!
					</Text>
				</ImageBackground>
				<ImageBackground style={styles.pic} source={picKhajit}>
					<Text style={styles.userName}>
						khajiit has wares
					</Text>
				</ImageBackground>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'space-around' 
	},
	pic: {
		flex: 1,
		width: Dimensions.get('window').width,
		resizeMode: 'cover',
		justifyContent: 'flex-end',
		alignItems: 'flex-end',
		borderRadius: 100,
		margin: StyleSheet.hairlineWidth
	},
	userName: {
		fontSize: 30,
		backgroundColor: 'rgba(0,0,0,0.5)',
		color: 'white',
		padding: 10
	}
});

AppRegistry.registerComponent('SampleApp', () => App);